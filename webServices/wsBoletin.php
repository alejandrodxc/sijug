<?php session_start();
	
	include("latis/conexionBD.php");
	include_once("latis/nusoap/nusoap.php");
	include_once("latis/sgjp/siajop.php");
	include_once("latis/latisErrorHandler.php");
	ini_set('default_socket_timeout', 160000);
	ini_set('post_max_size', '1024M');
	ini_set('upload_max_filesize', '1024M');

	function registrarPublicacionBoletinMasivo($arrPublicaciones)
	{
		global $con;
	
		$arrResultados=array();
		try
		{
			$aPublicaciones=json_decode($arrPublicaciones);
			foreach($aPublicaciones as $p)
			{
				try
				{
					$resultado=registrarPublicacionBoletin($p->idAplicacion,$p->idDocumentoPublicacion,$p->fechaPublicacion,$p->numeroPublicacion);
					$oResultado=json_decode($resultado);
					$oResultadoPublicacion=array();
					$oResultadoPublicacion["idDocumentoPublicacion"]=$p->idDocumentoPublicacion;
					$oResultadoPublicacion["resultado"]=$oResultado->respuesta;
					$oResultadoPublicacion["mensajeError"]=$oResultado->mensajeError;
					
					array_push($arrResultados,$oResultadoPublicacion);
				}
				catch(Exception $e)
				{
					$oResultadoPublicacion=array();
					$oResultadoPublicacion["idDocumentoPublicacion"]=$p->idDocumentoPublicacion;
					$oResultadoPublicacion["resultado"]=0;
					$oResultadoPublicacion["mensajeError"]=$e->getMessage();
					
					array_push($arrResultados,$oResultadoPublicacion);
				}
				
			}
			
			return '{"respuesta":"1","arrResultados":'.json_encode($arrResultados).',"mensajeError":""}';
	
		}
		catch(Exception $e)
		{
			return '{"respuesta":"0","mensajeError":"'.cv($e->getMessage()).'"}';
			
		}
	}
	
	function registrarPublicacionEdictoMasivo($arrPublicaciones)
	{
		global $con;
	
		$arrResultados=array();
		try
		{
			$aPublicaciones=json_decode($arrPublicaciones);
			foreach($aPublicaciones as $p)
			{
				try
				{
					$resultado=registrarPublicacionEdicto($p->idAplicacion,$p->idDocumentoPublicacion,$p->fechaPublicacion,$p->numeroPublicacion);
					$oResultado=json_decode($resultado);
					$oResultadoPublicacion=array();
					$oResultadoPublicacion["idDocumentoPublicacion"]=$p->idDocumentoPublicacion;
					$oResultadoPublicacion["resultado"]=$oResultado->respuesta;
					$oResultadoPublicacion["mensajeError"]=$oResultado->mensajeError;
					
					array_push($arrResultados,$oResultadoPublicacion);
				}
				catch(Exception $e)
				{
					$oResultadoPublicacion=array();
					$oResultadoPublicacion["idDocumentoPublicacion"]=$p->idDocumentoPublicacion;
					$oResultadoPublicacion["resultado"]=0;
					$oResultadoPublicacion["mensajeError"]=$e->getMessage();
					
					array_push($arrResultados,$oResultadoPublicacion);
				}
				
			}
			
			return '{"respuesta":"1","arrResultados":'.json_encode($arrResultados).',"mensajeError":""}';
	
		}
		catch(Exception $e)
		{
			return '{"respuesta":"0","mensajeError":"'.cv($e->getMessage()).'"}';
			
		}
	}
	
	function registrarPublicacionEdicto($idAplicacion,$idDocumentoPublicacion,$fechaPublicacion,$numeroPublicacion)
	{
		global $con;
		$idFolioSolicitud="";
		try
		{
			$_SESSION["idUsr"]=2390;
			$_SESSION["idRol"]="'-100_0'";
			$_SESSION["codigoUnidad"]="001";
			$_SESSION["codigoInstitucion"]="001";	
			
			
			$consulta="INSERT INTO 8000_bitacoraNotificacionDeBoletin(fechaNotificacion,documentoXML,resultado,tipoPublicacion) 
						VALUES('".date("Y-m-d H:i:s")."','".bE('{"fechaPublicacion":"'.$fechaPublicacion.'","idAplicacion":"'.$idAplicacion.'","idDocumentoPublicacion":"'.$idDocumentoPublicacion.
						'"}')."',0,2)";
			$con->ejecutarConsulta($consulta);
			$idFolioSolicitud=$con->obtenerUltimoID();
			
			$consulta="SELECT idFormulario FROM 900_formularios WHERE categoriaFormulario=1";
			$iFormularioMaterias=$con->obtenerValor($consulta);
			
			$consulta="SELECT claveOPC,materia,idCsDocs,ipServidor,puerto FROM _".$iFormularioMaterias."_tablaDinamica WHERE idCsDocs=".$idAplicacion;
			$fMateria=$con->obtenerPrimeraFila($consulta);
			if(!$fMateria)
			{
				
				$consulta="UPDATE 8000_bitacoraNotificacionDeBoletin SET resultado=0,mensaje='La clave de materia NO existe' 
						WHERE idNotificacion=".$idFolioSolicitud;
				$con->ejecutarConsulta($consulta);
				$xmlResp='{"respuesta":"0","mensajeError":"La clave de materia NO existe"}';				  
				return $xmlResp;
			}
	
			if($fMateria[3]=='127.0.0.1')
			{
				return '{"respuesta":"1","mensajeError":""}';	
						
				/*$regEncontrado=false;
				$consulta="SELECT idFormulario FROM 900_formularios WHERE categoriaFormulario='Edicto'";
				$rFormularios=$con->obtenerFilas($consulta);
				while($fFormulario=mysql_fetch_row($rFormularios))
				{
					$consulta="SELECT id__".$fFormulario[0]."_tablaDinamica FROM _".$fFormulario[0]."_tablaDinamica WHERE idPublicacion=".$idDocumentoPublicacion;
					$iRegistro=$con->obtenerValor($consulta);
					if($iRegistro!="")
					{
						$consulta="UPDATE _".$fFormulario[0]."_tablaDinamica SET fechaPublicacion ='".$fechaPublicacion.
								"',noPublicacion=".$numeroPublicacion." WHERE id__".$fFormulario[0]."_tablaDinamica=".$iRegistro;
						
						$con->ejecutarConsulta($consulta);
						
						$consulta="UPDATE 8000_bitacoraNotificacionDeBoletin SET resultado=1,iFormulario=".$fFormulario[0].
								",iRegistro=".$iRegistro.",mensaje='' WHERE idNotificacion=".$idFolioSolicitud;
						
						$con->ejecutarConsulta($consulta);
						
						cambiarEtapaFormulario($fFormulario[0],$iRegistro,3,"",-1,"NULL","NULL",0);
						return '{"respuesta":"1","mensajeError":""}';	
						
						
					}
				}
				$consulta="UPDATE 8000_bitacoraNotificacionDeBoletin SET resultado=0,mensaje='ID de publicaci&oacute;n NO encontrada' 
						WHERE idNotificacion=".$idFolioSolicitud;
				$con->ejecutarConsulta($consulta);
				return '{"respuesta":"0","mensajeError":"ID de publicaci&oacute;n NO encontrada"}';*/
				
			}
			else
			{
				$urlWebServices="http://".$fMateria[3].":".$fMateria[4]."/webServices/wsBoletin.php?wsdl";
				$client = new nusoap_client($urlWebServices,"wsdl"); 
				$parametros=array();
				$parametros["idAplicacion"]=$idAplicacion;
				$parametros["idDocumentoPublicacion"]=$idDocumentoPublicacion;
				$parametros["fechaPublicacion"]=$fechaPublicacion;
				$parametros["numeroPublicacion"]=$numeroPublicacion;
				$docXML = $client->call("registrarPublicacionEdicto", $parametros);
				
				return $docXML;
				
			}
		}
		catch(Exception $e)
		{
			return '{"respuesta":"0","mensajeError":"'.cv($e->$e->getMessage()).'"}';
		}
	}
	
	function registrarPublicacionBoletin($idAplicacion,$idDocumentoPublicacion,$fechaPublicacion,$numeroPublicacion)
	{
		global $con;
		$idFolioSolicitud="";
		try
		{
			$_SESSION["idUsr"]=2390;
			$_SESSION["idRol"]="'-100_0'";
			$_SESSION["codigoUnidad"]="001";
			$_SESSION["codigoInstitucion"]="001";	
			
			
			$consulta="INSERT INTO 8000_bitacoraNotificacionDeBoletin(fechaNotificacion,documentoXML,resultado,tipoPublicacion) 
						VALUES('".date("Y-m-d H:i:s")."','".bE('{"fechaPublicacion":"'.$fechaPublicacion.'","idAplicacion":"'.$idAplicacion.'","idDocumentoPublicacion":"'.$idDocumentoPublicacion.
						'"}')."',0,1)";
			$con->ejecutarConsulta($consulta);
			$idFolioSolicitud=$con->obtenerUltimoID();
			
			$consulta="SELECT idFormulario FROM 900_formularios WHERE categoriaFormulario=1";
			$iFormularioMaterias=$con->obtenerValor($consulta);
			
			$consulta="SELECT claveOPC,materia,idCsDocs,ipServidor,puerto FROM _".$iFormularioMaterias."_tablaDinamica WHERE idCsDocs=".$idAplicacion;
			$fMateria=$con->obtenerPrimeraFila($consulta);
			if(!$fMateria)
			{
				
				$consulta="UPDATE 8000_bitacoraNotificacionDeBoletin SET resultado=0,mensaje='La clave de materia NO existe' 
						WHERE idNotificacion=".$idFolioSolicitud;
				$con->ejecutarConsulta($consulta);
				$xmlResp='{"respuesta":"0","mensajeError":"La clave de materia NO existe"}';				  
				return $xmlResp;
			}
	
			if($fMateria[3]=='127.0.0.1')
			{
				$regEncontrado=false;
				$consulta="SELECT idFormulario FROM 900_formularios WHERE categoriaFormulario='Boletin'";
				$rFormularios=$con->obtenerFilas($consulta);
				while($fFormulario=mysql_fetch_row($rFormularios))
				{
					$consulta="SELECT id__".$fFormulario[0]."_tablaDinamica FROM _".$fFormulario[0]."_tablaDinamica WHERE idPublicacion=".$idDocumentoPublicacion;
					$iRegistro=$con->obtenerValor($consulta);
					if($iRegistro!="")
					{
						$consulta="UPDATE _".$fFormulario[0]."_tablaDinamica SET fechaPublicacion ='".$fechaPublicacion.
								"',noPublicacion=".$numeroPublicacion." WHERE id__".$fFormulario[0]."_tablaDinamica=".$iRegistro;
						
						$con->ejecutarConsulta($consulta);
						
						$consulta="UPDATE 8000_bitacoraNotificacionDeBoletin SET resultado=1,iFormulario=".$fFormulario[0].
								",iRegistro=".$iRegistro.",mensaje='' WHERE idNotificacion=".$idFolioSolicitud;
						
						$con->ejecutarConsulta($consulta);
						
						cambiarEtapaFormulario($fFormulario[0],$iRegistro,3,"",-1,"NULL","NULL",0);
						return '{"respuesta":"1","mensajeError":""}';	
						
						
					}
				}
				$consulta="UPDATE 8000_bitacoraNotificacionDeBoletin SET resultado=0,mensaje='ID de publicaci&oacute;n NO encontrada' 
						WHERE idNotificacion=".$idFolioSolicitud;
				$con->ejecutarConsulta($consulta);
				return '{"respuesta":"0","mensajeError":"ID de publicaci&oacute;n NO encontrada"}';
				
			}
			else
			{
				$urlWebServices="http://".$fMateria[3].":".$fMateria[4]."/webServices/wsBoletin.php?wsdl";
				$client = new nusoap_client($urlWebServices,"wsdl"); 
				$parametros=array();
				$parametros["idAplicacion"]=$idAplicacion;
				$parametros["idDocumentoPublicacion"]=$idDocumentoPublicacion;
				$parametros["fechaPublicacion"]=$fechaPublicacion;
				$parametros["numeroPublicacion"]=$numeroPublicacion;
				$docXML = $client->call("registrarPublicacionBoletin", $parametros);
				
				return $docXML;
				
			}
		}
		catch(Exception $e)
		{
			
			return '{"respuesta":"0","mensajeError":"'.cv($e->getMessage()).'"}';
			
			
		}
	}
	
	

	$arrParam=array();
	$server = new soap_server;
	$ns=$urlSitio."/webServices";
	$server->configurewsdl('ApplicationServices',$ns);
	$server->wsdl->schematargetnamespace=$ns;
	if($tipoMateria!="CC")
	{
		$server->register('registrarPublicacionBoletin',array('idAplicacion'=>'xsd:string','idDocumentoPublicacion'=>'xsd:string','fechaPublicacion'=>'xsd:string','numeroPublicacion'=>'xsd:string'),array('return' => 'xsd:string'),$ns,false,'rpc','encoded','');
		$server->register('registrarPublicacionEdicto',array('idAplicacion'=>'xsd:string','idDocumentoPublicacion'=>'xsd:string','fechaPublicacion'=>'xsd:string','numeroPublicacion'=>'xsd:string'),array('return' => 'xsd:string'),$ns,false,'rpc','encoded','');
	}
	else
	{
		$server->register('registrarPublicacionEdictoMasivo',array('arrPublicaciones'=>'xsd:string'),array('return' => 'xsd:string'),$ns,false,'rpc','encoded','');
		$server->register('registrarPublicacionBoletinMasivo',array('arrPublicaciones'=>'xsd:string'),array('return' => 'xsd:string'),$ns,false,'rpc','encoded','');	
	}
	if (isset($HTTP_RAW_POST_DATA)) 
	{
		$input = $HTTP_RAW_POST_DATA;
	}
	else 
	{
		$input = implode("rn", file('php://input'));
	}
	
	
	$server->service($input);
?>