<?php 
	include("latis/conexionBD.php");
	include("latis/nusoap/nusoap.php");
	ini_set('default_socket_timeout', 160000);
	
	$llaveWS="f2d7f9d22621cacf2ffd093d2e845f15";
	
	$consulta="INSERT INTO  1000_bitacoraAccesoSW(fecha,idFuncion,pruebas) VALUES('".date("Y-m-d H:i:s")."',0,0) ";
	$con->ejecutarConsulta($consulta);
	
	function buscarPacienteCampaniaPorFolio($folio,$llave)
	{
		global $con;
		global $llaveWS;
		
		$consulta="INSERT INTO  1000_bitacoraAccesoSW(fecha,idFuncion,pruebas) VALUES('".date("Y-m-d H:i:s")."',1,0) ";
		$con->ejecutarConsulta($consulta);
		
		$cad="";
		if($llave==$llaveWS)
		{
			$arrFilas=array();
			
			
			$consulta="SELECT id__1022_tablaDinamica,apPaterno,apMaterno,nombres,folio,fechaNacimiento,'100' as puntuacion FROM _1022_tablaDinamica WHERE folio='".cv($folio)."'";
			$fila=$con->obtenerPrimeraFila($consulta);
			if($fila)
			{
				array_push($arrFilas,$fila);
			}
			$arrPacientes="";
			foreach($arrFilas as $fila)
			{
				$o="<paciente>".
						"<apPaterno>".htmlspecialchars($fila[1])."</apPaterno>".
						"<apMaterno>".htmlspecialchars($fila[2])."</apMaterno>".
						"<nombre>".htmlspecialchars($fila[3])."</nombre>".
						"<idPaciencieOncoAsesor>".($fila[0])."</idPaciencieOncoAsesor>".
						"<folioPacienteCampania>".($fila[4])."</folioPacienteCampania>".
						"<fechaNacimiento>".($fila[5])."</fechaNacimiento>".
						"<porcentajeMatch>".($fila[6])."</porcentajeMatch>".
					"</paciente>";
	
				$arrPacientes.=$o;
	
					
			}
			
			$cad=	"<?xml version=\"1.0\" encoding=\"UTF-8\"?>".
					"<pacientesBusqueda>".
						$arrPacientes.
					"</pacientesBusqueda>";
		}
		else
		{
			$cad=	"<?xml version=\"1.0\" encoding=\"UTF-8\"?>".
					"<error>".
						"Valor de llave errónea".
					"</error>";
		
			
		}
		return new soapval('return', 'xsd:string', $cad);
		
	}
	
	function buscarPacienteCampania($apPaterno,$apMaterno,$nombre,$fechaNacimiento,$llave)
	{
		global $con;
		global $llaveWS;
		$arrPacientes='';
		
		$consulta="INSERT INTO  1000_bitacoraAccesoSW(fecha,idFuncion,pruebas) VALUES('".date("Y-m-d H:i:s")."',2,0) ";
		$con->ejecutarConsulta($consulta);
		
		$cad="";
		
		if($llave==$llaveWS)
		{
			$arrFilas=array();
			$consulta="SELECT id__1022_tablaDinamica,apPaterno,apMaterno,nombres,folio,fechaNacimiento,'100' as puntuacion  FROM _1022_tablaDinamica WHERE apPaterno='".cv($apPaterno)."' AND apMaterno='".cv($apMaterno)."' AND nombres='".cv($nombre)."' AND fechaNacimiento='".$fechaNacimiento."'";
			$fila=$con->obtenerPrimeraFila($consulta);
			if($fila)
			{
				array_push($arrFilas,$fila);
			}
			else
			{
				$consulta="SELECT id__1022_tablaDinamica,apPaterno,apMaterno,nombres,folio,fechaNacimiento,MATCH(apPaterno, apMaterno,nombres) AGAINST ('".cv($apPaterno." ".$apMaterno." ".$nombre)."') AS puntuacion 
							FROM _1022_tablaDinamica WHERE MATCH(apPaterno, apMaterno,nombres) AGAINST ('".cv($apPaterno." ".$apMaterno." ".$nombre)."') ORDER BY apPaterno,apMaterno,nombres";
				$res=$con->obtenerFilas($consulta);
				while($fila=mysql_fetch_row($res))
				{
					$fila[6]*=10;
					array_push($arrFilas,$fila);
				}
			}
			
			
			
			foreach($arrFilas as $fila)
			{
				$o="<paciente>".
						"<apPaterno>".htmlspecialchars($fila[1])."</apPaterno>".
						"<apMaterno>".htmlspecialchars($fila[2])."</apMaterno>".
						"<nombre>".htmlspecialchars($fila[3])."</nombre>".
						"<idPaciencieOncoAsesor>".($fila[0])."</idPaciencieOncoAsesor>".
						"<folioPacienteCampania>".($fila[4])."</folioPacienteCampania>".
						"<fechaNacimiento>".($fila[5])."</fechaNacimiento>".
						"<porcentajeMatch>".($fila[6])."</porcentajeMatch>".
					"</paciente>";
	
				$arrPacientes.=$o;
	
					
			}
			
			$cad=	"<?xml version=\"1.0\" encoding=\"UTF-8\"?>".
					"<pacientesBusqueda>".
						$arrPacientes.
					"</pacientesBusqueda>";
		}
		else
		{
			$cad=	"<?xml version=\"1.0\" encoding=\"UTF-8\"?>".
					"<error>".
						"Valor de llave errónea".
					"</error>";
		
		}
		
		
		return new soapval('return', 'xsd:string', $cad);
		
		
	}
	
	function asociarPacienteCampaniaExpedienteClinico($idPaciencieOncoAsesor,$idPacienteExpedienteClinico,$llave)
	{
		global $con;
		global $llaveWS;
		
		$consulta="INSERT INTO  1000_bitacoraAccesoSW(fecha,idFuncion,pruebas) VALUES('".date("Y-m-d H:i:s")."',3,0) ";
		$con->ejecutarConsulta($consulta);
		
		if($llave==$llaveWS)
		{
			$consulta="select count(*) from _1022_tablaDinamica where id__1022_tablaDinamica=".$idPaciencieOncoAsesor;
			$nReg=$con->obtenerValor($consulta);
			if($nReg>0)
			{
				$consulta="UPDATE _1022_tablaDinamica SET IDExpedienteINCAN=".$idPacienteExpedienteClinico." WHERE id__1022_tablaDinamica=".$idPaciencieOncoAsesor;
				if($con->ejecutarConsulta($consulta))
					return 1;
				return 0;
			}
			return -2;
			
			
			
		}
		return -1;
	}
	
	function registrarPacienteCampania($apPaterno,$apMaterno,$nombre,$fechaNacimiento,$IdExpedienteClinico,$llave)
	{
		global $con;
		global $llaveWS;
		
		$consulta="INSERT INTO  1000_bitacoraAccesoSW(fecha,idFuncion,pruebas) VALUES('".date("Y-m-d H:i:s")."',4,0) ";
		$con->ejecutarConsulta($consulta);
		
		
		if($llave==$llaveWS)
		{
			$fNacimiento="NULL";
			$anios=0;
			if($fechaNacimiento!="")
			{
				$fNacimiento="'".$fechaNacimiento."'";
				$anioNac=date("Y",strtotime($fechaNacimiento));
				$fechaActual=date("Y-m-d");
				$anioActual=date("Y",strtotime($fechaActual));                         
				$anios= $anioActual-$anioNac;                                   
				$fechaBaseActual=$anioActual."-".date("m",strtotime($fechaNacimiento))."-".date("d",strtotime($fechaNacimiento));
				if(strtotime($fechaBaseActual)>strtotime($fechaActual))                      
					$anios--;				                                   
			}
			$x=0;
			$consulta[$x]="begin";
			$x++;
			$consulta[$x]="INSERT INTO _1022_tablaDinamica(fechaCreacion,responsable,idEstado,apPaterno,apMaterno,nombres,fechaNacimiento,anios,IDExpedienteINCAN,registradoPorINCAN)
						VALUES('".date("Y-m-d H:i:s")."',1,10,'".cv($apPaterno)."','".cv($apMaterno)."','".cv($nombre)."',".$fNacimiento.",".$anios.",".$IdExpedienteClinico.",1)";
			$x++;			
			$consulta[$x]="set @idRegistro:=(select last_insert_id())";
			$x++;
			$consulta[$x]="update _1022_tablaDinamica set codigo=@idRegistro where id__1022_tablaDinamica=@idRegistro";
			$x++;
						
			$consulta[$x]="commit";
			$x++;
			if($con->ejecutarBloque($consulta))
			{
				$query="select @idRegistro";
				$idRegistro=$con->obtenerValor($query);
				return $idRegistro;
			}	
			return 0;
		}
		else
			return -1;
		
	}
	
	$arrParam=array();
	$server = new soap_server;
	$ns=$urlSitio."/webServices";
	$server->configurewsdl('ApplicationServices',$ns);
	$server->wsdl->schematargetnamespace=$ns;

	$server->register('buscarPacienteCampania',array('apPaterno'=>'xsd:string','apMaterno'=>'xsd:string','nombre'=>'xsd:string','fechaNacimiento'=>'xsd:string','llave'=>'xsd:string'),array('return' => 'xsd:string'),$ns,false,'rpc','encoded','Buscar paciente participante de la campaña');
	$server->register('asociarPacienteCampaniaExpedienteClinico',array('idPaciencieOncoAsesor'=>'xsd:int','idPacienteExpedienteClinico'=>'xsd:int','llave'=>'xsd:string'),array('return' => 'xsd:int'),$ns,false,'rpc','encoded','Asociar paciente participante de la campaña con expediente clínico');
	$server->register('registrarPacienteCampania',array('apPaterno'=>'xsd:string','apMaterno'=>'xsd:string','nombre'=>'xsd:string','fechaNacimiento'=>'xsd:string','IdExpedienteClinico'=>'xsd:int','llave'=>'xsd:string'),array('return' => 'xsd:int'),$ns,false,'rpc','encoded','Registrar paciente participante de la campaña');
	$server->register('buscarPacienteCampaniaPorFolio',array('folio'=>'xsd:string','llave'=>'xsd:string'),array('return' => 'xsd:string'),$ns,false,'rpc','encoded','Buscar paciente participante de la campaña por folio de la campaña');
	
	if (isset($HTTP_RAW_POST_DATA)) 
	{
		$input = $HTTP_RAW_POST_DATA;
	}
	else 
	{
		$input = implode("rn", file('php://input'));
	}
	$server->service($input);
	

	
?>	